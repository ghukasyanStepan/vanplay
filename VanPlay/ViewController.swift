//
//  LibraryViewController.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 29.03.22.
//

import UIKit

class LibraryViewController: UIViewController {

    var viewModel: LibraryViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        viewModel = LibraryViewModel(delegate: self)
        viewModel.loadData()
    }


}

extension ViewController: LibraryViewModelDelegate {
    func didLoaded(items: [LibraryContentModel]) {
        print(items)
    }
}
