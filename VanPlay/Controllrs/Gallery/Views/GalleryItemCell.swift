//
//  GalleryItemCell.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 31.03.22.
//

import UIKit
import SDWebImage

class GalleryItemCell: UITableViewCell {

    // MARK: - Properties

    private var viewModel: GalleryItemCellViewModel!

    // MARK: - Outlets

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    
    // MARK: - Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }
    
    // MARK: - Methods

    func update(viewModel: GalleryItemCellViewModel) {
        self.viewModel = viewModel
        setData()
    }
}

// MARK: - Private Methods

private extension GalleryItemCell {

    func configureUI() {
        itemImageView.layer.cornerRadius = 12
        itemImageView.clipsToBounds = true
        containerView.layer.cornerRadius = 12
    }
    
    func setData() {
        nameLabel.text = viewModel.title
        itemImageView.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
        itemImageView.sd_setImage(with: viewModel.coverPhotoUrl, placeholderImage:
                            nil, options: .continueInBackground)
    }
}

