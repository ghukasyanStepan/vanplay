//
//  GalleryViewController.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 31.03.22.
//

import UIKit

class GalleryViewController: UIViewController {

    // MARK: - Outlets

    @IBOutlet var vcObject: GalleryObject!
    
    // MARK: - Properties
    
    var viewModel: GalleryViewModel?

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        title = "Gallery"
    }
}

// MARK: - Private Methods

private extension GalleryViewController {
    
    func configure() {
        vcObject.configure()
        configureViewModel()
    }
    
    func configureViewModel() {
        self.viewModel?.loadData()
    }
}

// MARK: - Details View Model Delegate

extension GalleryViewController: GalleryViewModelDelegate {
    
    func didLoaded(items: [GalleryItemCellViewModel]) {
        vcObject.update(items: items)
    }
}
