//
//  GalleryObject.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 31.03.22.
//

import UIKit

typealias GalleryTableDataSource = UITableViewDiffableDataSource<Int, GalleryItemCellViewModel>
typealias GalleryTableDataSourceSnapshot = NSDiffableDataSourceSnapshot<Int, GalleryItemCellViewModel>

class GalleryObject: NSObject {

    // MARK: - Outlets

    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties

    private var items: [GalleryItemCellViewModel] = []
    private var datasource: GalleryTableDataSource?
    
    // MARK: - Methods
    
    func configure() {
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.register(GalleryItemCell.nib, forCellReuseIdentifier: GalleryItemCell.id)
        configureDataSource()
    }
    
    func update(items: [GalleryItemCellViewModel]) {
        self.items = items
        reloadData()
    }
}

// MARK: - Private Methods

private extension GalleryObject {
    
    func configureDataSource() {
        datasource = GalleryTableDataSource(tableView: tableView, cellProvider: { (tableView, indexPath, viewModel) -> UITableViewCell? in
            guard let cell = tableView.dequeueReusableCell(withIdentifier: GalleryItemCell.id, for: indexPath) as? GalleryItemCell else {return UITableViewCell()}
            cell.update(viewModel: viewModel)
            return cell
        })
    }
    
    func snapshotForCurrentState() -> GalleryTableDataSourceSnapshot {
        var snapshot = GalleryTableDataSourceSnapshot()
        snapshot.appendSections([0])
        snapshot.appendItems(items)
        return snapshot
    }
        
    func reloadData(animatingDifferences: Bool = false) {
        let snapshot = snapshotForCurrentState()
        apply(snapshot: snapshot, animatingDifferences: animatingDifferences)
    }
    
    func apply(snapshot: GalleryTableDataSourceSnapshot, animatingDifferences: Bool = false) {
        datasource?.apply(snapshot, animatingDifferences: animatingDifferences)
    }
}
