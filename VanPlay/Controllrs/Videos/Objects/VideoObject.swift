//
//  VideoObject.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 31.03.22.
//

import UIKit

typealias VideoTableDataSource = UITableViewDiffableDataSource<Int, VideoItemCellViewModel>
typealias VideoTableDataSourceSnapshot = NSDiffableDataSourceSnapshot<Int, VideoItemCellViewModel>

class VideoObject: NSObject {

    // MARK: - Outlets

    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties

    private var items: [VideoItemCellViewModel] = []
    private var datasource: VideoTableDataSource?
    
    // MARK: - Methods
    
    func configure() {
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.register(VideoItemCell.nib, forCellReuseIdentifier: VideoItemCell.id)
        configureDataSource()
    }
    
    func update(items: [VideoItemCellViewModel]) {
        self.items = items
        reloadData()
    }
}

// MARK: - Private Methods

private extension VideoObject {
    
    func configureDataSource() {
        datasource = VideoTableDataSource(tableView: tableView, cellProvider: { (tableView, indexPath, viewModel) -> UITableViewCell? in
            guard let cell = tableView.dequeueReusableCell(withIdentifier: VideoItemCell.id, for: indexPath) as? VideoItemCell else {return UITableViewCell()}
            cell.update(viewModel: viewModel)
            return cell
        })
    }
    
    func snapshotForCurrentState() -> VideoTableDataSourceSnapshot {
        var snapshot = VideoTableDataSourceSnapshot()
        snapshot.appendSections([0])
        snapshot.appendItems(items)
        return snapshot
    }
        
    func reloadData(animatingDifferences: Bool = false) {
        let snapshot = snapshotForCurrentState()
        apply(snapshot: snapshot, animatingDifferences: animatingDifferences)
    }
    
    func apply(snapshot: VideoTableDataSourceSnapshot, animatingDifferences: Bool = false) {
        datasource?.apply(snapshot, animatingDifferences: animatingDifferences)
    }
}
