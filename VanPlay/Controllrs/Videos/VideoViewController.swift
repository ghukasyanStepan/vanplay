//
//  VideoViewController.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 31.03.22.
//

import UIKit

class VideoViewController: UIViewController {

    // MARK: - Outlets

    @IBOutlet var vcObject: VideoObject!
    
    // MARK: - Properties
    
    var viewModel: VideoViewModel?

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        title = "Videos"
    }
}

// MARK: - Private Methods

private extension VideoViewController {
    
    func configure() {
        vcObject.configure()
        configureViewModel()
    }
    
    func configureViewModel() {
        self.viewModel?.loadData()
    }
}

// MARK: - Details View Model Delegate

extension VideoViewController: VideoViewModelDelegate {
    
    func didLoaded(items: [VideoItemCellViewModel]) {
        vcObject.update(items: items)
    }
}
