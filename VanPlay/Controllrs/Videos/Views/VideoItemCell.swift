//
//  VideoItemCell.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 31.03.22.
//

import UIKit
import SDWebImage
import youtube_ios_player_helper

class VideoItemCell: UITableViewCell {

    // MARK: - Properties

    private var viewModel: VideoItemCellViewModel!
    
    // MARK: - Outlets

    @IBOutlet weak var videoView: YTPlayerView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    
    // MARK: - Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }
    
    // MARK: - Methods

    func update(viewModel: VideoItemCellViewModel) {
        self.viewModel = viewModel
        setData()
    }
}

// MARK: - Private Methods

private extension VideoItemCell {

    func configureUI() {
        itemImageView.layer.cornerRadius = 12
        itemImageView.clipsToBounds = true
        containerView.layer.cornerRadius = 12
    }
    
    func setData() {
        nameLabel.text = viewModel.title
        itemImageView.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
        itemImageView.sd_setImage(with: viewModel.coverPhotoUrl, placeholderImage:
                            nil, options: .continueInBackground)
        videoView.load(withVideoId: viewModel.videoId!)
    }
}
