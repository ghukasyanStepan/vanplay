//
//  DetailItem.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 31.03.22.
//

import Foundation

enum DetailItem: Hashable {
    
    case image(ItemImageCellViewModel)
    case field(TextInfoCellViewModel)
    case contetItem(ContentItemsCellViewModel)
}
