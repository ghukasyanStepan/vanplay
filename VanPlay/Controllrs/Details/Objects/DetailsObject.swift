//
//  DetailsObject.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 30.03.22.
//

import UIKit

typealias DetailsObjectTableDataSource = UITableViewDiffableDataSource<Int, DetailItem>
typealias DetailsObjectTableDataSourceSnapshot = NSDiffableDataSourceSnapshot<Int, DetailItem>

protocol DetailsObjectDelegate: AnyObject {
    func didSelectGallery()
    func didSelectVideos()
}

class DetailsObject: NSObject {

    // MARK: - Outlets

    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    
    private var items: [DetailItem] = []
    private var delegate: DetailsObjectDelegate?
    private var datasource: DetailsObjectTableDataSource?

    // MARK: - Methods
    
    func configure(delegate: DetailsObjectDelegate?) {
        self.delegate = delegate
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.register(DetailsImageViewCell.nib, forCellReuseIdentifier: DetailsImageViewCell.id)
        tableView.register(TextInfoCell.nib, forCellReuseIdentifier: TextInfoCell.id)
        tableView.register(ContentItemsCell.nib, forCellReuseIdentifier: ContentItemsCell.id)
        configureDataSource()
    }
    
    func update(items: [DetailItem]) {
        self.items = items
        reloadData()
    }
    
    func reloadItems(items: [DetailItem], animatingDifferences: Bool = false) {
        guard var snapshot = datasource?.snapshot() else {return}
        snapshot.reloadItems(items)
        apply(snapshot: snapshot, animatingDifferences: animatingDifferences)
    }
}

// MARK: - Private Methods

private extension DetailsObject {
    
    func configureDataSource() {
        datasource = DetailsObjectTableDataSource(tableView: tableView, cellProvider: { (tableView, indexPath, item) -> UITableViewCell? in
            
            switch item {
            case .field(let viewModel):
                guard let cell = tableView.dequeueReusableCell(withIdentifier: TextInfoCell.id, for: indexPath) as? TextInfoCell else {return UITableViewCell()}
                cell.update(viewModel: viewModel, delegate: self)
                return cell

            case.image(let viewModel):
                guard let cell = tableView.dequeueReusableCell(withIdentifier: DetailsImageViewCell.id, for: indexPath) as? DetailsImageViewCell else {return UITableViewCell()}
                cell.update(viewModel: viewModel)
                return cell
            case .contetItem(let viewModel):
                guard let cell = tableView.dequeueReusableCell(withIdentifier: ContentItemsCell.id, for: indexPath) as? ContentItemsCell else {return UITableViewCell()}
                cell.update(viewModel: viewModel, delegate: self)
                return cell
            }
        })
    }
    
    func snapshotForCurrentState() -> DetailsObjectTableDataSourceSnapshot {
        var snapshot = DetailsObjectTableDataSourceSnapshot()
        snapshot.appendSections([0])
        snapshot.appendItems(items)
        return snapshot
    }
        
    func reloadData(animatingDifferences: Bool = false) {
        let snapshot = snapshotForCurrentState()
        apply(snapshot: snapshot, animatingDifferences: animatingDifferences)
    }
    
    func apply(snapshot: DetailsObjectTableDataSourceSnapshot, animatingDifferences: Bool = false) {
        datasource?.apply(snapshot, animatingDifferences: animatingDifferences)
    }
}

// MARK: - Table View Delegate

extension DetailsObject: TextInfoCellDelegate {
    func didSelect(isExpanded: Bool) {
        reloadData()
    }
}

extension DetailsObject: ContentItemsCellDelegate {
    func didSelectGallery() {
        delegate?.didSelectGallery()
    }
    
    func didSelectVideos() {
        delegate?.didSelectVideos()
    }
}
