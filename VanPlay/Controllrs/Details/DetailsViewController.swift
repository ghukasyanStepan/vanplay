//
//  DetailsViewController.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 30.03.22.
//

import UIKit

class DetailsViewController: BaseViewController {

    // MARK: - Outlets

    @IBOutlet var vcObject: DetailsObject!

    // MARK: - Properties
    
    var viewModel: DetailsViewModel?

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        title = "Details"
    }
}

// MARK: - Private Methods

private extension DetailsViewController {
    
    func configure() {
        vcObject.configure(delegate: self)
        configureViewModel()
    }
    
    func configureViewModel() {
        self.viewModel?.loadData()
    }
}

// MARK: - Details View Model Delegate

extension DetailsViewController: DetailsViewModelDelegate {
    
    func didLoaded(items: [DetailItem]) {
        vcObject.update(items: items)
    }
}

extension DetailsViewController: DetailsObjectDelegate {
    
    func didSelectGallery() {
        guard let items = viewModel?.fetchItem().gallery else { return }
        if let viewController = storyboard?.instantiateViewController(withIdentifier: GalleryViewController.id) as? GalleryViewController {
            viewController.viewModel = GalleryViewModel(delegate: viewController, models: items)
            navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func didSelectVideos() {
        guard let items = viewModel?.fetchItem().videos else { return }
        if let viewController = storyboard?.instantiateViewController(withIdentifier: VideoViewController.id) as? VideoViewController {
            viewController.viewModel = VideoViewModel(delegate: viewController, models: items)
            navigationController?.pushViewController(viewController, animated: true)
        }
    }
}
