//
//  DetailsImageViewCell.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 30.03.22.
//

import UIKit
import SDWebImage

class DetailsImageViewCell: UITableViewCell {

    // MARK: - Properties

    private var viewModel: ItemImageCellViewModel!

    // MARK: - Outlets

    @IBOutlet weak var itemImageView: UIImageView!
    
    // MARK: - Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }
    
    // MARK: - Methods

    func update(viewModel: ItemImageCellViewModel) {
        self.viewModel = viewModel
        setData()
    }
}

// MARK: - Private Methods

private extension DetailsImageViewCell {

    func configureUI() {
        itemImageView.layer.cornerRadius = 12
        itemImageView.clipsToBounds = true
    }
    
    func setData() {
        itemImageView.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
        itemImageView.sd_setImage(with: viewModel.fetchImageUrl(), placeholderImage:
                            nil, options: .continueInBackground)
    }
}
