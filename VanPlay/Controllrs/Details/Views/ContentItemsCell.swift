//
//  ContentItemsCell.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 31.03.22.
//

import UIKit

protocol ContentItemsCellDelegate: AnyObject {
    func didSelectGallery()
    func didSelectVideos()
}

class ContentItemsCell: UITableViewCell {

    // MARK: - Outlets

    @IBOutlet weak var videosButton: UIButton!
    @IBOutlet weak var galleryButton: UIButton!
    
    // MARK: - Properties
    
    private var viewModel: ContentItemsCellViewModel!
    private weak var delegate: ContentItemsCellDelegate?
    
    // MARK: - Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }

    func update(viewModel: ContentItemsCellViewModel, delegate: ContentItemsCellDelegate?) {
        self.viewModel = viewModel
        self.delegate = delegate
        galleryButton.isHidden = !self.viewModel.isGallery
        videosButton.isHidden = !self.viewModel.isVideos
    }

    @IBAction func galleryButtonAction(_ sender: UIButton) {
        delegate?.didSelectGallery()
    }
    
    @IBAction func videosButtonAction(_ sender: UIButton) {
        delegate?.didSelectVideos()
    }
}

// MARK: - Private Methods

private extension ContentItemsCell {

    func configureUI() {
        videosButton.layer.cornerRadius = 12
        galleryButton.layer.cornerRadius = 12
    }
}
