//
//  TextInfoCell.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 30.03.22.
//

import UIKit

protocol TextInfoCellDelegate: AnyObject {
    func didSelect(isExpanded: Bool)
}

class TextInfoCell: UITableViewCell {
    
    // MARK: - Outlets
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var expandView: UIView!
    @IBOutlet weak var expandButton: UIButton!
    
    // MARK: - Properties

    private var viewModel: TextInfoCellViewModel!
    private weak var delegate: TextInfoCellDelegate?
        
    // MARK: - Methods

    func update(viewModel: TextInfoCellViewModel, delegate: TextInfoCellDelegate?) {
        self.delegate = delegate
        self.viewModel = viewModel
        setData()
    }

    // MARK: Methods
    
    @IBAction func expandedButtonAction(_ sender: UIButton) {
        sender.isSelected.toggle()
        titleLabel.numberOfLines = sender.isSelected ? 0 : 3
        self.delegate?.didSelect(isExpanded: sender.isSelected)
    }
}

// MARK: - Private Methods

private extension TextInfoCell {
        
    func setData() {
        titleLabel.text = viewModel.fetchText()
        expandView.isHidden = !viewModel.isExpandable
        titleLabel.numberOfLines = expandButton.isSelected ? 0 : 3
    }
}

