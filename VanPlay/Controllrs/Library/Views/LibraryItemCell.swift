//
//  LibraryItemCell.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 30.03.22.
//

import UIKit
import SDWebImage

class LibraryItemCell: UITableViewCell {

    // MARK: - Properties

    private var viewModel: LibraryItemCellViewModel!

    // MARK: - Outlets

    @IBOutlet weak var checkIcon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    
    // MARK: - Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }
    
    // MARK: - Methods

    func update(viewModel: LibraryItemCellViewModel) {
        self.viewModel = viewModel
        setData()
    }
}

// MARK: - Private Methods

private extension LibraryItemCell {

    func configureUI() {
        itemImageView.layer.cornerRadius = 12
        itemImageView.clipsToBounds = true
        containerView.layer.cornerRadius = 12
    }
    
    func setData() {
        checkIcon.isHidden = !viewModel.isAllreadySeen
        categoryLabel.text = viewModel.category
        titleLabel.text = viewModel.title
        dateLabel.text = viewModel.date
        itemImageView.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
        itemImageView.sd_setImage(with: viewModel.coverPhotoUrl, placeholderImage:
                            nil, options: .continueInBackground)
    }
}
