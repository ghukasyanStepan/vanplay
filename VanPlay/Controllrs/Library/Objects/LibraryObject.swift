//
//  LibraryObject.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 30.03.22.
//

import UIKit

typealias LibraryTableDataSource = UITableViewDiffableDataSource<Int, LibraryItemCellViewModel>
typealias LibraryTableDataSourceSnapshot = NSDiffableDataSourceSnapshot<Int, LibraryItemCellViewModel>

protocol LibraryObjectDelegate: AnyObject {
    func didSelectItem(item: LibraryItemCellViewModel)
}

class LibraryObject: NSObject {

    // MARK: - Outlets

    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties

    private var items: [LibraryItemCellViewModel] = []
    private weak var delegate: LibraryObjectDelegate?
    private var datasource: LibraryTableDataSource?
    
    // MARK: - Methods
    
    func configure(delegate: LibraryObjectDelegate?) {
        self.delegate = delegate
        
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.register(LibraryItemCell.nib, forCellReuseIdentifier: LibraryItemCell.id)
        configureDataSource()
    }
    
    func update(items: [LibraryItemCellViewModel]) {
        self.items = items
        reloadData()
    }
}

// MARK: - Private Methods

private extension LibraryObject {
    
    func configureDataSource() {
        datasource = LibraryTableDataSource(tableView: tableView, cellProvider: { (tableView, indexPath, viewModel) -> UITableViewCell? in
            guard let cell = tableView.dequeueReusableCell(withIdentifier: LibraryItemCell.id, for: indexPath) as? LibraryItemCell else {return UITableViewCell()}
            cell.update(viewModel: viewModel)
            return cell
        })
    }
    
    func snapshotForCurrentState() -> LibraryTableDataSourceSnapshot {
        var snapshot = LibraryTableDataSourceSnapshot()
        snapshot.appendSections([0])
        snapshot.appendItems(items)
        return snapshot
    }
        
    func reloadData(animatingDifferences: Bool = false) {
        let snapshot = snapshotForCurrentState()
        apply(snapshot: snapshot, animatingDifferences: animatingDifferences)
    }
    
    func apply(snapshot: LibraryTableDataSourceSnapshot, animatingDifferences: Bool = false) {
        datasource?.apply(snapshot, animatingDifferences: animatingDifferences)
    }
}

// MARK: - Table View Delegate

extension LibraryObject: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = datasource?.itemIdentifier(for: indexPath)
        guard let item = item else { return }
        delegate?.didSelectItem(item: item)
    }
}
