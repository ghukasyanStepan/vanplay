//
//  LibraryViewController.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 29.03.22.
//

import UIKit

class LibraryViewController: UIViewController {

    // MARK: - Outlets

    @IBOutlet weak var vcObject: LibraryObject!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - Properties

    private var viewModel: LibraryViewModel?

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.navigationBar.isHidden = false
    }
}

// MARK: - Private Methods

private extension LibraryViewController {
    
    func configure() {
        vcObject.configure(delegate: self)
        configureViewModel()
    }
    
    func configureViewModel() {
        self.viewModel = LibraryViewModel(delegate: self)
        bindViewModel()
        self.activityIndicator.startAnimating()
        self.viewModel?.loadData()
    }
    
    func bindViewModel() {
        viewModel?.onError = {[weak self] error in
            guard let self = self else {return}
            self.activityIndicator.stopAnimating()
            Dialog.showError(message: error)
        }
    }
}

// MARK: - Library View Model Delegate

extension LibraryViewController: LibraryViewModelDelegate {
    
    func didLoaded(items: [LibraryItemCellViewModel]) {
        self.activityIndicator.stopAnimating()
        vcObject.update(items: items)
    }
}

// MARK: - Library Object Delegate

extension LibraryViewController: LibraryObjectDelegate {
        
    func didSelectItem(item: LibraryItemCellViewModel) {
        if let viewController = storyboard?.instantiateViewController(withIdentifier: DetailsViewController.id) as? DetailsViewController {
            var arr = AppManager.shared.seenItemIndexes
            arr.append(item.fetchId)
            arr.removeDuplicates()
            AppManager.shared.seenItemIndexes = arr
            viewController.viewModel = DetailsViewModel(delegate: viewController, item: item)
            vcObject.tableView.reloadData()
            showDetails(controller: viewController)
        }
    }
    
    func showDetails(controller: UIViewController) {
        if UIDevice.current.userInterfaceIdiom == .pad {
            if let split = self.navigationController?.appRootController() as? UISplitViewController {
                let nvc = UINavigationController(rootViewController: controller)
                split.showDetailViewController(nvc, sender: nil)
            }
        } else {
            navigationController?.pushViewController(controller, animated: true)
        }
    }
}
