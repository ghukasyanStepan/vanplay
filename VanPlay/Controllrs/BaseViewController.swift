//
//  BaseViewController.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 31.03.22.
//

import UIKit

class BaseViewController: UIViewController {
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = UIColor(named: "darkGray")
        navigationController?.navigationBar.configAppearance()
        view.clipsToBounds = true
    }

    func createBackItem() {
        let leftItem = navigationItem(image: UIImage(named: "icBack"), target: self, action: #selector(btnBackTap), position: .left)
        navigationItem.leftBarButtonItem = leftItem
    }
    
    @objc func btnBackTap() {
        navigationController?.popViewController(animated: true)
    }
}
