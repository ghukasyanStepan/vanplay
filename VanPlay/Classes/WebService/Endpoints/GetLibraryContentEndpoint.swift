//
//  GetLibraryContentEndpoint.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 30.03.22.
//

import Foundation
import Alamofire

struct GetLibraryContentEndpoint: Endpointable {
    
    var files: Files?
    var method: HTTPMethod = .get
    var path: String = "/feed"
    var data: Data?
}
