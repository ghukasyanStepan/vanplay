//
//  WebServiceError.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 30.03.22.
//

import Foundation
import Alamofire

public enum WebServiceError: Error {
    case noInternet(endPoint: Endpointable)
    case serverError(statusCode: HTTPStatusCode, body: Data?, response: HTTPURLResponse)
    case clientError(statusCode: HTTPStatusCode, body: Data?, response: HTTPURLResponse)
    case undefined(statusCode: HTTPStatusCode, body: Data?, response: HTTPURLResponse)
    case dataParsing(error: Error)
    case message(message: String)
    case noStatusCode
}

extension WebServiceError {
  var displayMessage: String? {
    switch self {
    case .clientError(statusCode: _, body: let data, response: _):
      if let error = DataDecoder<String>.decode(for: data) {
        return error
      }
    case .serverError(statusCode: _, body: let data, response: _):
        if let error = DataDecoder<String>.decode(for: data) {
          return error
        }
    case .undefined(statusCode: _, body: let data, response: _):
        if let error = DataDecoder<String>.decode(for: data) {
          return error
        }
    case .message(message: let error):
        return error
    case .dataParsing(error: let error):
        return error.localizedDescription
    case .noInternet(endPoint: _):
        return "No Internet Connection"
    case .noStatusCode:
        return "No Internet Connection"
    }
    return nil
  }
}

extension Encodable {
    
    func toData() -> Data? {
        let data = try? JSONEncoder().encode(self)
        return data
    }
}

extension String {
    static func jsonString(from object: Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: [.fragmentsAllowed]) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    static func jsonString(from data: Data) -> String? {
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    static func jsonString<T: Codable>(codable: T) -> String {
        var str = ""
        let encoder = JSONEncoder()
        let data = try? encoder.encode(codable)
        if let data = data {
            str = String(data: data, encoding: .utf8) ?? ""
        }
        return str
    }
    
}

extension Data {
    static func data(from object: Any?) -> Data? {
        guard let object = object else {
            return nil
        }
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: [.fragmentsAllowed]) else {
            return nil
        }
        return data
    }
    
    func decode<T: Codable>() -> T? {
        do {
            let decoder = JSONDecoder()
            
            let result = try decoder.decode(T.self, from: self)
            return result
            
        } catch {
            print(error)
            return nil
        }
    }
}

struct DataDecoder<T: Codable> {
    static func decode(for object: Any?) -> T? {
        do {
            let decoder = JSONDecoder()
            
            guard let data = Data.data(from: object) else {return nil}
             
            let result = try decoder.decode(T.self, from: data)
            return result
            
        } catch {
            print(error)
            return nil
        }
    }
  
  static func decode(for data: Data?) -> T? {
      do {
          let decoder = JSONDecoder()
          
          guard let data = data else {return nil}
           
          let result = try decoder.decode(T.self, from: data)
          return result
          
      } catch {
          print(error)
          return nil
      }
  }
}
