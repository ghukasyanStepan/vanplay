//
//  WebNetworkLayer.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 01.04.22.
//

import Foundation
import Alamofire

final class WebNetworkLayer: NSObject {
    static func configEndpointParams(token: String? = nil) {
        
        Network.prepareReachability()
        updateEndpointParams(token: token)
    }
    
    static func updateEndpointParams(token: String? = nil) {
        EndpointDefaultConfigs.scheme = "http"
        EndpointDefaultConfigs.host = AppCommonParams.BaseURL
        
        var header = HTTPHeaders([HTTPHeader.contentType("application/json"),
                                  HTTPHeader.accept("application/json")])
        if let token = token {
            header.add(HTTPHeader.apiToken(token))
        }
        
        EndpointDefaultConfigs.onTopHeaders  = header.dictionary
    }
}

extension HTTPHeader {
    public static func apiToken(_ value: String) -> HTTPHeader {
        HTTPHeader(name: "X-API-TOKEN", value: value)
    }
}
