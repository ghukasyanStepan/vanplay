//
//  UIBarButtonItem.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 31.03.22.
//

import UIKit

extension UIBarButtonItem {
    func configAppearance() {
        tintColor = .white
        var titleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17),
                                   NSAttributedString.Key.foregroundColor: UIColor.white]
        setTitleTextAttributes(titleTextAttributes, for: .normal)
        setTitleTextAttributes(titleTextAttributes, for: .selected)
        titleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17),
                                   NSAttributedString.Key.foregroundColor: UIColor(white: 1, alpha: 0.5)]
        setTitleTextAttributes(titleTextAttributes, for: .disabled)
    }
}
