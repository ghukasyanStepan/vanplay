//
//  UIViewController.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 31.03.22.
//

import UIKit

enum AppNavigationItemPosition {
    case left
    case right
}

extension UIViewController {
    func navigationItem(title: String?, style: UIBarButtonItem.Style = .plain, target: Any? = self, action: Selector?, position: AppNavigationItemPosition) -> UIBarButtonItem {
        
        let item = UIBarButtonItem(title: title, style: style, target: target, action: action)
        item.configAppearance()
        switch position {
        case .left:
            navigationItem.leftBarButtonItem = item
        case .right:
            navigationItem.rightBarButtonItem = item
        }
        return item
    }
    
    func navigationItem(image: UIImage?, style: UIBarButtonItem.Style = .plain, target: Any? = self, action: Selector?, position: AppNavigationItemPosition) -> UIBarButtonItem {
        
        let item = UIBarButtonItem(image: image, style: style, target: target, action: action)
        item.configAppearance()
        switch position {
        case .left:
            navigationItem.leftBarButtonItem = item
        case .right:
            navigationItem.rightBarButtonItem = item
        }
        return item
    }
}
