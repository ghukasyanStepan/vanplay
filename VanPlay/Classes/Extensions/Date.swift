//
//  Date.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 30.03.22.
//

import Foundation

extension Date {
    
    func dateStringMDYY() -> String {
        return string(for: "MM/dd/yyyy")
    }
    
    func dateLongString() -> String {
        return string(for: "MMM dd, yyyy")
    }
        
    func string(for dateFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: self)
    }
}
