//
//  UINavigationBar.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 31.03.22.
//

import UIKit

extension UINavigationBar {
    func configAppearance() {
        setBackgroundImage(UIImage(), for: .default)
        let attributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 17),
                               NSAttributedString.Key.foregroundColor: UIColor.white]
        
        titleTextAttributes = attributes
        isTranslucent = false
        
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = barTintColor
        appearance.shadowColor = barTintColor
        appearance.titleTextAttributes = attributes
        tintColor = .white
        standardAppearance = appearance
        scrollEdgeAppearance = appearance
    }
}
