//
//  NSObject.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 30.03.22.
//

import Foundation

import UIKit

public extension NSObject {

    static var id: String {
        return String(describing: self)
    }
        
    func appRootController() ->UIViewController? {
        return NSObject.appRootController()
    }
    
    static func appRootController() ->UIViewController? {
        if let rootViewController = UIApplication.shared.keyWindow?.rootViewController {
            return rootViewController
        }
        return nil
    }
    
    static func appCurrentController() ->UIViewController? {
        return fetchCurrentController(appRootController())
    }
    
    func appCurrentController() ->UIViewController? {
        return NSObject.appCurrentController()
    }
    
    static func fetchCurrentController(_ rootController : UIViewController?) ->UIViewController? {
        var currentController = rootController
        if let rootController = rootController as? UINavigationController {
            let viewControllers = (rootController).viewControllers
            currentController = viewControllers.last
            currentController = fetchCurrentController(currentController)
        }
        if let rootController = rootController as? UITabBarController {
            currentController = rootController.selectedViewController
            currentController = fetchCurrentController(currentController)
        }
        if ((rootController?.presentedViewController) != nil) {
            currentController = rootController?.presentedViewController
            currentController = fetchCurrentController(currentController)
        }
        
        return currentController
    }
}

