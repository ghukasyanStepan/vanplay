//
//  UIView.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 30.03.22.
//

import UIKit

extension UIView {
    static var nib: UINib {
        return UINib(nibName: self.id, bundle: nil)
    }
}
