//
//  Array.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 31.03.22.
//

import Foundation

extension Array where Element: Equatable {
    @discardableResult
    mutating func removeDuplicates() -> [Element] {
        self = reduce(into: [Element]()) {
            if !$0.contains($1) {
                $0.append($1)
            }
        }
        return self
    }
}
