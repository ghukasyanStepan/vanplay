//
//  Dialog.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 30.03.22.
//

import UIKit

final class Dialog {
    
    
    static func showError(title: String? = nil, message: String?, onClose: @escaping Closure = {}) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            if (title?.isEmpty ?? true) && (message?.isEmpty ?? true) {
                return
            }
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            alert.addAction(.init(title: "Close", style: .default, handler: { (_) in
                onClose()
            }))
            alert.show()
        }
    }
}

