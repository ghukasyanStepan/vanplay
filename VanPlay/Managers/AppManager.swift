//
//  AppManager.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 31.03.22.
//

import Foundation

final class AppManager {
    
    static let shared = AppManager()
    
    private init () { }
    
    var seenItemIndexes: [Int] {
        get {
            UserDefaults.standard.array(forKey: "seenIndexes") as? [Int] ?? []
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "seenIndexes")
        }
    }
}
