//
//  RealmService.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 01.04.22.
//

import Foundation
import RealmSwift

class RealmService {

    private init() {
        Realm.Configuration.defaultConfiguration = Realm.Configuration(schemaVersion: 1, migrationBlock: RealmService.migrate)
    }

    static let shared = RealmService()

    var realm = try! Realm()

    func create<T: Object>(_ object: T) {
        do {
            try realm.write {
                realm.add(object)
            }
        } catch {
            print(error)
        }
    }

    func delete<T: Object>(_ object: T) {
        do {
            try realm.write {
                realm.delete(object)
            }
        } catch {
            print(error)
        }
    }

    func deleteAll() {
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    static func migrate(migration: Migration, oldVersion: UInt64) {
        
    }
}
