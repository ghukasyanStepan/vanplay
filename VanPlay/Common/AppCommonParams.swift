//
//  AppCommonParams.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 30.03.22.
//

import Foundation

struct AppCommonParams {
    
    static var BaseURL: String {
        return "coinstats.getsandbox.com"
    }
    
    static var apiPath: String {
        return "/api"
    }
}

