//
//  Closures.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 30.03.22.
//

import Foundation

typealias ClosureWithText = ((String?) -> Void)
typealias Closure = (() -> Void)
typealias ErrorClosure = ((String?) -> Void)

typealias AppResponse<T: Codable> = Result<T, WebServiceError>
typealias Response<T: Codable> = AppResponse<BaseResponseModel<T>>
