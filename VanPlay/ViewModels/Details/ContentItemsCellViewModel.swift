//
//  ContentItemsCellViewModel.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 31.03.22.
//

import Foundation

class ContentItemsCellViewModel: Hashable {
        
    func hash(into hasher: inout Hasher) {
        hasher.combine(isGalleryShow)
        hasher.combine(isVideosShow)
    }
    
    static func == (lhs: ContentItemsCellViewModel, rhs: ContentItemsCellViewModel) -> Bool {
        return lhs.isVideosShow == rhs.isVideosShow && lhs.isGalleryShow == rhs.isGalleryShow
    }

    private var isGalleryShow: Bool
    private var isVideosShow: Bool
    
    init(isGalleryShow: Bool, isVideosShow: Bool) {
        self.isVideosShow = isVideosShow
        self.isGalleryShow = isGalleryShow
    }
    
    var isVideos: Bool {
        return isVideosShow
    }

    var isGallery: Bool {
        return isGalleryShow
    }
}
