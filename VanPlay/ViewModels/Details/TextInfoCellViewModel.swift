//
//  TextInfoCellViewModel.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 31.03.22.
//

import Foundation

class TextInfoCellViewModel: Hashable {
    
    private var text: String
    var isExpandable: Bool
    
    init(text: String, isExpandable: Bool = false) {
        self.isExpandable = isExpandable
        self.text = text
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(text)
    }
    
    static func == (lhs: TextInfoCellViewModel, rhs: TextInfoCellViewModel) -> Bool {
        return lhs.text == rhs.text
    }
    
    func fetchText() -> String {
        return text
    }
}
