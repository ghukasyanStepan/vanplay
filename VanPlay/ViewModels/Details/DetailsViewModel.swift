//
//  DetailsViewModel.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 30.03.22.
//

import Foundation

protocol DetailsViewModelDelegate: AnyObject {
    func didLoaded(items: [DetailItem])
}

class DetailsViewModel {
    
    // MARK: - Properties
        
    private weak var delegate: DetailsViewModelDelegate?
    private var item: LibraryItemCellViewModel
    private var items = [DetailItem]()
    
    // MARK: - Init
    
    init(delegate: DetailsViewModelDelegate, item: LibraryItemCellViewModel) {
        self.delegate = delegate
        self.item = item
    }

    // MARK: - Methods
    
    func loadData() {
        
        items = []
        
        items.append(.image(ItemImageCellViewModel(imageUrl: item.coverPhotoUrl)))
        
        var isVideo = false
        var isGallery = false
        if let videos = item.videos, !videos.isEmpty {
            isVideo = true
        }
        
        if let gallery = item.gallery, !gallery.isEmpty {
            isGallery = true
        }

        if isVideo || isGallery {
            items.append(.contetItem(ContentItemsCellViewModel(isGalleryShow: isGallery, isVideosShow: isVideo)))
        }
        
        if let title = item.title {
            items.append(.field(TextInfoCellViewModel(text: title)))
        }

        if let category = item.category {
            items.append(.field(TextInfoCellViewModel(text: category)))
        }
        
        if let date = item.date {
            items.append(.field(TextInfoCellViewModel(text: date)))
        }
        
        if let body = item.body {
            items.append(.field(TextInfoCellViewModel(text: body, isExpandable: true)))
        }
        
        delegate?.didLoaded(items: items)
    }
    
    func fetchItem() -> LibraryItemCellViewModel {
        return item
    }
}
