//
//  ItemImageCellViewModel.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 31.03.22.
//

import Foundation

class ItemImageCellViewModel: Hashable {
    
    private var imageUrl: URL?
    
    init(imageUrl: URL?) {
        
        self.imageUrl = imageUrl
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(imageUrl)
    }
    
    static func == (lhs: ItemImageCellViewModel, rhs: ItemImageCellViewModel) -> Bool {
        return lhs.imageUrl == rhs.imageUrl
    }
    
    func fetchImageUrl() -> URL? {
        return  imageUrl
    }
}
