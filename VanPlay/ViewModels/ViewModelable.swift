//
//  ViewModelable.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 30.03.22.
//

import Foundation

protocol ViewModelable {
    var service: DataRequestable.Type {get}
    var onError: ClosureWithText? {get}
}

extension ViewModelable {
    var service: DataRequestable.Type {
        return NetworkLayer.self
    }
}
