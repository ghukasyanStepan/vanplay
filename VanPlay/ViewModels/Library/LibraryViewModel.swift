//
//  LibraryViewModel.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 30.03.22.
//

import Foundation
import RealmSwift

protocol LibraryViewModelDelegate: AnyObject {
    func didLoaded(items: [LibraryItemCellViewModel])
}

class LibraryViewModel: ViewModelable {
    
    // MARK: - Properties
    
    var onError: ClosureWithText?
    
    private weak var delegate: LibraryViewModelDelegate?
    
    private var items: [LibraryItemCellViewModel] = []
    
    // MARK: - Init
    
    init(delegate: LibraryViewModelDelegate) {
        self.delegate = delegate
    }

    func loadData() {
        let endpoint = GetLibraryContentEndpoint()
        service.request(with: endpoint) {[weak self] (response: Response<[LibraryContentModel]>) in
            guard let self = self else {return}
            switch response {
            case .success(let result):
                if result.isOk {
                    if let models = result.data {
                        self.configCellViewModels(models: models)
                        self.saveRealmItems(models: models)
                        self.delegate?.didLoaded(items: self.items)
                    }
                }
            case .failure(let error):
                self.getItems()
                self.onError?(error.displayMessage)
            }
        }
    }
}

private extension LibraryViewModel {
    func configCellViewModels(models: [LibraryContentModel]) {
        models.enumerated().forEach { index, model in
            var mod = model
            mod.identifier = index
            self.items.append(LibraryItemCellViewModel(model: mod))
        }
    }
    
    func saveRealmItems(models: [LibraryContentModel]) {
        RealmService.shared.deleteAll()
        let itemss = List<RealmLibraryContentModel>()
        for model in models {
            itemss.append(RealmLibraryContentModel(model: model))
        }
        let realmModel = RealmModel(model: itemss)
        RealmService.shared.create(realmModel)
    }
    
    func getItems() {
        let realm = RealmService.shared.realm
        if let models = realm.objects(RealmModel.self).first?.models {
            var modelArr = [LibraryContentModel]()
            for item in models {
                
                let iiitem = LibraryContentModel(realmModel: item)
                modelArr.append(iiitem)
            }
            configCellViewModels(models: modelArr)
            self.delegate?.didLoaded(items: items)
        }
    }
}
