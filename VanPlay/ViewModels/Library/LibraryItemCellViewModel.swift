//
//  LibraryItemCellViewModel.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 30.03.22.
//

import Foundation

class LibraryItemCellViewModel: Hashable {
    
    private var model: LibraryContentModel
    
    init(model: LibraryContentModel) {

        self.model = model
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(model.identifier)
    }
    
    static func == (lhs: LibraryItemCellViewModel, rhs: LibraryItemCellViewModel) -> Bool {
        return lhs.model.identifier == rhs.model.identifier
    }
    
    func fetchModel() -> LibraryContentModel {
        return model
    }
    
    var coverPhotoUrl: URL? {
        return URL(string: model.coverPhotoUrl ?? "")
    }
    
    var title: String? {
        return model.title
    }
    
    var category: String? {
        return model.category
    }
    
    var date: String? {
        return configDate()
    }
    
    var body: String? {
        return model.body
    }
    
    var videos: [VideoModel]? {
        return model.video
    }
    
    var gallery: [GalleryModel]? {
        return model.gallery
    }
    
    var fetchId: Int {
        return model.identifier ?? 0
    }

    private func configDate() -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(model.date ?? 0))
        return date.dateLongString() 
    }
    
    var isAllreadySeen: Bool {
        return AppManager.shared.seenItemIndexes.contains(fetchId)
    }
}
