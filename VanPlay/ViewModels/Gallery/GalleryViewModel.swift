//
//  GalleryViewModel.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 31.03.22.
//

import Foundation

protocol GalleryViewModelDelegate: AnyObject {
    func didLoaded(items: [GalleryItemCellViewModel])
}

class GalleryViewModel {
    
    // MARK: - Properties
        
    private weak var delegate: GalleryViewModelDelegate?
    private var items = [GalleryItemCellViewModel]()
    private var models = [GalleryModel]()
    
    // MARK: - Init
    
    init(delegate: GalleryViewModelDelegate, models: [GalleryModel]) {
        self.delegate = delegate
        self.models = models
    }

    // MARK: - Methods
    
    func loadData() {
        
        items = []
        models.forEach { items.append(GalleryItemCellViewModel(model: $0))}
        delegate?.didLoaded(items: items)
    }
}
