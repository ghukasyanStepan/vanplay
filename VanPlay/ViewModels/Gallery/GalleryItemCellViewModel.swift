//
//  GalleryItemCellViewModel.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 31.03.22.
//

import Foundation

class GalleryItemCellViewModel: Hashable {
    
    private var model: GalleryModel
    
    init(model: GalleryModel) {

        self.model = model
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(model)
    }
    
    static func == (lhs: GalleryItemCellViewModel, rhs: GalleryItemCellViewModel) -> Bool {
        return lhs.model == rhs.model
    }
    
    func fetchModel() -> GalleryModel {
        return model
    }
    
    var coverPhotoUrl: URL? {
        return URL(string: model.contentUrl ?? "")
    }
    
    var title: String? {
        return model.title
    }
}
