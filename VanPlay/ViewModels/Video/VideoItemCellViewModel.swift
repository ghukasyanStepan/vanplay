//
//  VideoItemCellViewModel.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 31.03.22.
//

import Foundation

class VideoItemCellViewModel: Hashable {
    
    private var model: VideoModel
    
    init(model: VideoModel) {

        self.model = model
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(model)
    }
    
    static func == (lhs: VideoItemCellViewModel, rhs: VideoItemCellViewModel) -> Bool {
        return lhs.model == rhs.model
    }
    
    func fetchModel() -> VideoModel {
        return model
    }
    
    var coverPhotoUrl: URL? {
        return URL(string: model.thumbnailUrl ?? "")
    }
    
    var videoId: String? {
        return model.youtubeId
    }
    
    var title: String? {
        return model.title
    }
}
