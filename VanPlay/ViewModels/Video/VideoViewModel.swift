//
//  VideoViewModel.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 31.03.22.
//

import Foundation

protocol VideoViewModelDelegate: AnyObject {
    func didLoaded(items: [VideoItemCellViewModel])
}

class VideoViewModel {
    
    // MARK: - Properties
    
    private weak var delegate: VideoViewModelDelegate?
    private var items = [VideoItemCellViewModel]()
    private var models = [VideoModel]()
    
    // MARK: - Init
    
    init(delegate: VideoViewModelDelegate?, models: [VideoModel]) {
        self.delegate = delegate
        self.models = models
    }
    
    // MARK: - Methods
    
    func loadData() {
        
        items = []
        models.forEach { items.append(VideoItemCellViewModel(model: $0))}
        delegate?.didLoaded(items: items)
    }
}
