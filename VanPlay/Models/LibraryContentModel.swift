//
//  LibraryItemModel.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 30.03.22.
//

import Foundation

struct LibraryContentModel: Codable, Hashable {
    var identifier: Int?
    var title: String?
    var category: String?
    var body: String?
    var shareUrl: String?
    var coverPhotoUrl: String?
    var date: Int?
    var gallery: [GalleryModel]?
    var video: [VideoModel]?
    
    init(realmModel: RealmLibraryContentModel) {
        self.identifier = realmModel.identifier.value
        self.title = realmModel.title
        self.category = realmModel.category
        self.body = realmModel.body
        self.shareUrl = realmModel.shareUrl
        self.coverPhotoUrl = realmModel.coverPhotoUrl
        self.date = realmModel.date.value
        
        var gallertItems = [GalleryModel]()
        for item in realmModel.gallery {
            gallertItems.append(GalleryModel(model: item))
        }
        self.gallery = gallertItems
        
        var videItems = [VideoModel]()
        for item in realmModel.video {
            videItems.append(VideoModel(model: item))
        }
        self.video = videItems
    }
}

struct GalleryModel: Codable, Hashable {
    var title: String?
    var thumbnailUrl: String?
    var contentUrl: String?
    
    init(model: RealmGalleryModel) {
        self.title = model.title
        self.thumbnailUrl = model.thumbnailUrl
        self.contentUrl = model.contentUrl
    }
}

struct VideoModel: Codable, Hashable {
    var title: String?
    var thumbnailUrl: String?
    var youtubeId: String?
    
    init(model: RealmVideoModel) {
        self.title = model.title
        self.thumbnailUrl = model.thumbnailUrl
        self.youtubeId = model.youtubeId
    }
}
