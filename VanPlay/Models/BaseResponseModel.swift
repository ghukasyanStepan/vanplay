//
//  BaseResponseModel.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 30.03.22.
//

import Foundation

struct BaseResponseModel<T: Codable>: Codable {
    let success: Bool?
    let data: T?
    let errors: [String]?
    
    var isOk: Bool {
        return success == true
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        success = try container.decode(Bool.self, forKey: .success)
        data = try? container.decode(T.self, forKey: .data)
        errors = try? container.decode([String].self, forKey: .errors)
    }
}
