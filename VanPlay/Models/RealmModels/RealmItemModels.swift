//
//  RealmItemModels.swift
//  VanPlay
//
//  Created by Stepan Ghukasyan on 01.04.22.
//

import Foundation
import RealmSwift

class RealmModel: Object {
    dynamic var models = List<RealmLibraryContentModel>()

    convenience init(model: List<RealmLibraryContentModel>) {
        self.init()
        models = model
    }
}

class RealmLibraryContentModel: Object {
    
    dynamic var identifier = RealmOptional<Int>()
    @objc dynamic var title: String?
    @objc dynamic var category: String?
    @objc dynamic var body: String?
    @objc dynamic var shareUrl: String?
    @objc dynamic var coverPhotoUrl: String?
    dynamic var date = RealmOptional<Int>()
    dynamic var gallery = List<RealmGalleryModel>()
    dynamic var video = List<RealmVideoModel>()
    
    convenience init(model: LibraryContentModel) {
        self.init()
        self.identifier.value = model.identifier
        self.title = model.title 
        self.category = model.category
        self.body = model.body
        self.shareUrl = model.shareUrl
        self.coverPhotoUrl = model.coverPhotoUrl
        self.date.value = model.date
        let gallertItems = List<RealmGalleryModel>()
        for item in model.gallery ?? [] {
            gallertItems.append(RealmGalleryModel(model: item))
        }
        self.gallery = gallertItems

        let videItems = List<RealmVideoModel>()
        for item in model.video ?? [] {
            videItems.append(RealmVideoModel(model: item))
        }
        self.video = videItems
    }
}

class RealmGalleryModel: Object {
    @objc dynamic var title: String?
    @objc dynamic var thumbnailUrl: String?
    @objc dynamic var contentUrl: String?

    required convenience init(model: GalleryModel) {
        self.init()
        self.title = model.title
        self.thumbnailUrl = model.thumbnailUrl
        self.contentUrl = model.contentUrl
    }
}

class RealmVideoModel: Object {
    @objc dynamic var title: String?
    @objc dynamic var thumbnailUrl: String?
    @objc dynamic var youtubeId: String?

    required convenience init(model: VideoModel) {
        self.init()
        self.title = model.title
        self.thumbnailUrl = model.thumbnailUrl
        self.youtubeId = model.youtubeId
    }
}
